import _ from 'lodash'
import {request} from '@/resources/_request'
import {RESOURCE_VARIABLE_PATH, RESOURCE_VARIABLE_EXTENDED_FIELDS} from '@/services/const'

// RESTful API for Variable
export function createVariable (params) {
  let method = 'post'
  let url = RESOURCE_VARIABLE_PATH
  return request(method, url, params)
}

export function indexVariable (params) {
  let method = 'get'
  let url = RESOURCE_VARIABLE_PATH
  _.assign(params, {fields: RESOURCE_VARIABLE_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function showVariable (params) {
  let method = 'get'
  let url = RESOURCE_VARIABLE_PATH
  _.assign(params, {fields: RESOURCE_VARIABLE_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function updateVariable (id, params) {
  let method = 'put'
  let url = RESOURCE_VARIABLE_PATH + '/' + id
  return request(method, url, params)
}

export function deleteVariable (id) {
  let method = 'delete'
  let url = RESOURCE_VARIABLE_PATH + '/' + id
  return request(method, url, null)
}
