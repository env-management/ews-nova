import {request} from '@/resources/_request'
import {RESOURCE_ENVIRONMENT_PATH} from '@/services/const'

// RESTful API for Environment
export function createEnvironment (params) {
  let method = 'post'
  let url = RESOURCE_ENVIRONMENT_PATH
  return request(method, url, params)
}

export function indexEnvironment (params) {
  let method = 'get'
  let url = RESOURCE_ENVIRONMENT_PATH
  return request(method, url, params)
}

export function showEnvironment (params) {
  let method = 'get'
  let url = RESOURCE_ENVIRONMENT_PATH
  return request(method, url, params)
}

export function updateEnvironment (id, params) {
  let method = 'put'
  let url = RESOURCE_ENVIRONMENT_PATH + '/' + id
  return request(method, url, params)
}

export function deleteEnvironment (id) {
  let method = 'delete'
  let url = RESOURCE_ENVIRONMENT_PATH + '/' + id
  return request(method, url, null)
}
