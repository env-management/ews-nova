import {request, requestFormData} from '@/resources/_request'
import {RESOURCE_ROLE_PATH} from '@/services/const'

// RESTful API for Role
export function createRole (params) {
  let method = 'post'
  let url = RESOURCE_ROLE_PATH
  return requestFormData(method, url, params)
}

export function indexRole (params) {
  let method = 'get'
  let url = RESOURCE_ROLE_PATH
  return request(method, url, params)
}

export function showRole (params) {
  let method = 'get'
  let url = RESOURCE_ROLE_PATH
  return request(method, url, params)
}

export function updateRole (id, params) {
  let method = 'put'
  let url = RESOURCE_ROLE_PATH + '/' + id
  return requestFormData(method, url, params)
}

export function deleteRole (id) {
  let method = 'delete'
  let url = RESOURCE_ROLE_PATH + '/' + id
  return request(method, url, null)
}
