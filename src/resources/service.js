import {request} from '@/resources/_request'
import {RESOURCE_SERVICE_PATH} from '@/services/const'

// RESTful API for Service
export function createService (params) {
  let method = 'post'
  let url = RESOURCE_SERVICE_PATH
  return request(method, url, params)
}

export function indexService (params) {
  let method = 'get'
  let url = RESOURCE_SERVICE_PATH
  return request(method, url, params)
}

export function showService (params) {
  let method = 'get'
  let url = RESOURCE_SERVICE_PATH
  return request(method, url, params)
}

export function updateService (id, params) {
  let method = 'put'
  let url = RESOURCE_SERVICE_PATH + '/' + id
  return request(method, url, params)
}

export function deleteService (id) {
  let method = 'delete'
  let url = RESOURCE_SERVICE_PATH + '/' + id
  return request(method, url, null)
}
