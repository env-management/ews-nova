import _ from 'lodash'
import {request, requestFormData} from '@/resources/_request'
import {RESOURCE_ACCOUNT_PATH, RESOURCE_ACCOUNT_EXTENDED_FIELDS} from '@/services/const'

// RESTful API for Account
export function createAccount (params) {
  let method = 'post'
  let url = RESOURCE_ACCOUNT_PATH
  return requestFormData(method, url, params)
}

export function indexAccount (params) {
  let method = 'get'
  let url = RESOURCE_ACCOUNT_PATH
  _.assign(params, {fields: RESOURCE_ACCOUNT_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function showAccount (params) {
  let method = 'get'
  let url = RESOURCE_ACCOUNT_PATH
  _.assign(params, {fields: RESOURCE_ACCOUNT_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function updateAccount (id, params) {
  let method = 'put'
  let url = RESOURCE_ACCOUNT_PATH + '/' + id
  return requestFormData(method, url, params)
}

export function deleteAccount (id) {
  let method = 'delete'
  let url = RESOURCE_ACCOUNT_PATH + '/' + id
  return request(method, url, null)
}
