import {request, requestFormData} from '@/resources/_request'
import {RESOURCE_ENTITY_PATH} from '@/services/const'

// RESTful API for Entity
export function createEntity (params) {
  let method = 'post'
  let url = RESOURCE_ENTITY_PATH
  return requestFormData(method, url, params)
}

export function indexEntity (params) {
  let method = 'get'
  let url = RESOURCE_ENTITY_PATH
  return request(method, url, params)
}

export function showEntity (params) {
  let method = 'get'
  let url = RESOURCE_ENTITY_PATH
  params.orders = [ 'namespace.asc', 'created_at.desc' ]
  return request(method, url, params)
}

export function updateEntity (id, params) {
  let method = 'put'
  let url = RESOURCE_ENTITY_PATH + '/' + id
  return requestFormData(method, url, params)
}

export function deleteEntity (id) {
  let method = 'delete'
  let url = RESOURCE_ENTITY_PATH + '/' + id
  return request(method, url, null)
}
