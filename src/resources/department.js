import _ from 'lodash'
import {request, requestFormData} from '@/resources/_request'
import {RESOURCE_DEPARTMENT_PATH, RESOURCE_DEPARTMENT_EXTENDED_FIELDS} from '@/services/const'

// RESTful API for Department
export function createDepartment (params) {
  let method = 'post'
  let url = RESOURCE_DEPARTMENT_PATH
  return request(method, url, params)
}

export function indexDepartment (params) {
  let method = 'get'
  let url = RESOURCE_DEPARTMENT_PATH
  _.assign(params, {fields: RESOURCE_DEPARTMENT_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function showDepartment (params) {
  let method = 'get'
  let url = RESOURCE_DEPARTMENT_PATH
  _.assign(params, {fields: RESOURCE_DEPARTMENT_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function updateDepartment (id, params) {
  let method = 'put'
  let url = RESOURCE_DEPARTMENT_PATH + '/' + id
  return requestFormData(method, url, params)
}

export function deleteDepartment (id) {
  let method = 'delete'
  let url = RESOURCE_DEPARTMENT_PATH + '/' + id
  return request(method, url, null)
}
