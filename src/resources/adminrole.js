import {request, requestFormData} from '@/resources/_request'
import {RESOURCE_ADMINROLE_PATH} from '@/services/const'

// RESTful API for Adminrole
export function createAdminrole (params) {
  let method = 'post'
  let url = RESOURCE_ADMINROLE_PATH
  return requestFormData(method, url, params)
}

export function indexAdminrole (params) {
  let method = 'get'
  let url = RESOURCE_ADMINROLE_PATH
  return request(method, url, params)
}

export function showAdminrole (params) {
  let method = 'get'
  let url = RESOURCE_ADMINROLE_PATH
  return request(method, url, params)
}

export function updateAdminrole (id, params) {
  let method = 'put'
  let url = RESOURCE_ADMINROLE_PATH + '/' + id
  return requestFormData(method, url, params)
}

export function deleteAdminrole (id) {
  let method = 'delete'
  let url = RESOURCE_ADMINROLE_PATH + '/' + id
  return request(method, url, null)
}
