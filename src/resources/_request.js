import axios from 'axios'
import {getCredentials} from '@/services/auth'

// create request for resource using axios
export function request (method, url, params) {
  return axios({
    method: method,
    url: url,
    responseType: 'json',
    headers: getCredentials(),
    params: params
  })
}

// create request for form data
export function requestFormData (method, url, formData) {
  return axios({
    method: method,
    url: url,
    responseType: 'json',
    headers: getCredentials(),
    data: formData,
    processData: false,
    contentType: 'multipart/form-data'
  })
}

// create request for return file
export function requestFile (method, url, params) {
  return axios({
    method: method,
    url: url,
    responseType: 'arraybuffer',
    headers: getCredentials(),
    params: params
  })
}
