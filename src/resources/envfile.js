import {requestFormData, request, requestFile} from '@/resources/_request'
import {RESOURCE_UPLOAD_ENVFILE_PATH, RESOURCE_SHOW_ENVFILE_PATH, RESOURCE_DOWNLOAD_ENVFILE_PATH} from '@/services/const'

export function uploadEnvFile (formData) {
  let method = 'post'
  let url = RESOURCE_UPLOAD_ENVFILE_PATH
  return requestFormData(method, url, formData)
}

export function showEnvFile () {
  let method = 'get'
  let url = RESOURCE_SHOW_ENVFILE_PATH
  return request(method, url, null)
}

export function downloadEnvFile (params) {
  let method = 'get'
  let url = RESOURCE_DOWNLOAD_ENVFILE_PATH
  return requestFile(method, url, params)
}
