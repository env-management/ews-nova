import _ from 'lodash'
import {request} from '@/resources/_request'
import {RESOURCE_RELEASE_PATH, RESOURCE_RELEASE_EXTENDED_FIELDS} from '@/services/const'

// RESTful API for Release
export function createRelease (params) {
  let method = 'post'
  let url = RESOURCE_RELEASE_PATH
  return request(method, url, params)
}

export function indexRelease (params) {
  let method = 'get'
  let url = RESOURCE_RELEASE_PATH
  _.assign(params, {fields: RESOURCE_RELEASE_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function showRelease (params) {
  let method = 'get'
  let url = RESOURCE_RELEASE_PATH
  _.assign(params, {fields: RESOURCE_RELEASE_EXTENDED_FIELDS})
  return request(method, url, params)
}

export function updateRelease (id, params) {
  let method = 'put'
  let url = RESOURCE_RELEASE_PATH + '/' + id
  return request(method, url, params)
}

export function deleteRelease (id) {
  let method = 'delete'
  let url = RESOURCE_RELEASE_PATH + '/' + id
  return request(method, url, null)
}
