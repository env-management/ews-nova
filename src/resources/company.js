import {request, requestFormData} from '@/resources/_request'
import {RESOURCE_COMPANY_PATH} from '@/services/const'

// RESTful API for Company
export function createCompany (params) {
  let method = 'post'
  let url = RESOURCE_COMPANY_PATH
  return requestFormData(method, url, params)
}

export function indexCompany (params) {
  let method = 'get'
  let url = RESOURCE_COMPANY_PATH
  return request(method, url, params)
}

export function showCompany (params) {
  let method = 'get'
  let url = RESOURCE_COMPANY_PATH
  return request(method, url, params)
}

export function updateCompany (id, params) {
  let method = 'put'
  let url = RESOURCE_COMPANY_PATH + '/' + id
  return requestFormData(method, url, params)
}

export function deleteCompany (id) {
  let method = 'delete'
  let url = RESOURCE_COMPANY_PATH + '/' + id
  return request(method, url, null)
}
