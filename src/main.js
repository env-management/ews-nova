// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'

// Use Frontend Framework ElementUI
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'

// Use Data Tables
import DataTablesServer from 'vue-data-tables'

// App.vue
import App from '@/App'

// Use router
import router from '@/router'

// Use Google OAuth2
import GAuth from 'vue-google-oauth2'

// Import Constants
import {GAUTH_OPTIONS} from '@/services/const'

Vue.use(ElementUI, { locale })
Vue.use(DataTablesServer)
Vue.use(GAuth, GAUTH_OPTIONS)

var main = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

export default main
