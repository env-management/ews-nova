import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/views/LogIn'
import Service from '@/views/Service'
import Environment from '@/views/Environment'
import Variable from '@/views/Variable'
import EnvFile from '@/views/EnvFile'
import Account from '@/views/Account'
import Department from '@/views/Department'
import Company from '@/views/Company'
import Role from '@/views/Role'
import {
  LOGIN_PATH,
  SERVICE_PATH, ENVIRONMENT_PATH, VARIABLE_PATH, ENVFILE_PATH,
  ACCOUNT_PATH, DEPARTMENT_PATH, COMPANY_PATH, ROLE_PATH,
  HOME_PATH } from '@/services/const'
import { redirectHomeWhenAuthenticated, checkAuthenticated } from '@/router/routing'

const routes = [
  {
    path: LOGIN_PATH,
    name: 'LogIn',
    component: LogIn
  },
  {
    path: HOME_PATH,
    name: 'Home',
    redirect: VARIABLE_PATH
  },
  {
    path: SERVICE_PATH,
    name: 'Service',
    component: Service
  },
  {
    path: ENVIRONMENT_PATH,
    name: 'Environment',
    component: Environment
  },
  {
    path: VARIABLE_PATH,
    name: 'Variable',
    component: Variable
  },
  {
    path: ENVFILE_PATH,
    name: 'EnvFile',
    component: EnvFile
  },
  {
    path: ACCOUNT_PATH,
    name: 'Account',
    component: Account
  },
  {
    path: DEPARTMENT_PATH,
    name: 'Department',
    component: Department
  },
  {
    path: COMPANY_PATH,
    name: 'Company',
    component: Company
  },
  {
    path: ROLE_PATH,
    name: 'Role',
    component: Role
  }
]

Vue.use(Router)
const router = new Router({
  // mode: 'history',
  routes: routes
})

router.beforeEach((to, from, next) => {
  if (to.path === LOGIN_PATH) {
    redirectHomeWhenAuthenticated(to, from, next)
  } else {
    checkAuthenticated(next)
  }
})

export default router
