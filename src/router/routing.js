import Router from 'vue-router'
import {isAuthenticated} from '@/services/auth'
import {LOGIN_PATH, HOME_PATH} from '@/services/const'

// Redirect to URL
export function redirect (routerPath) {
  window.location.replace(routerPath)
}

// Reload

export function reload (waitTime = 1000) {
  setTimeout(() => {
    window.history.go(0)
  }, waitTime)
}

// Redirect to Path
export function redirectPath (routerPath) {
  var router = new Router()
  router.go(routerPath)
  redirect('/#' + routerPath)
}

// Redirect Home when authenticated for LOGIN_PATH
export function redirectHomeWhenAuthenticated (to, from, next) {
  if (isAuthenticated()) {
    next(HOME_PATH)
  } else {
    next()
  }
}

// Check authenticated before routing
export function checkAuthenticated (next) {
  if (isAuthenticated()) {
    next()
  } else {
    next(LOGIN_PATH)
  }
}
