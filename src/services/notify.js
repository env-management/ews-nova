import _ from 'lodash'
import { Notification } from 'element-ui'

// Notify Success Messages
export function notifySuccess (message) {
  notify('success', message)
}

// Notify Error Messages
export function notifyError (message) {
  notify('error', message)
}

// Notify Warning Messages
export function notifyWarning (message) {
  notify('warning', message)
}

// Notify Info Messages
export function notifyInfo (message) {
  notify('info', message)
}

// Notify Message
function notify (type, message) {
  let title = type.charAt(0).toUpperCase() + type.slice(1)
  if (_.has(message, 'response')) {
    message = message.response
    if (_.has(message, 'data')) {
      message = message.data
      if (_.has(message, 'errors')) {
        message = message.errors
      }
    }
  }
  if (_.isArray(message)) {
    switch (message.length) {
      case 0:
        notifyString(type, title, '')
        break
      case 1:
        notifyString(type, title, message[0])
        break
      default:
        notifyArray(type, title, message)
    }
  } else if (_.isNil(message)) {
    notifyString(type, title, null)
  } else {
    notifyString(type, title, message)
  }
}

// Notify A String Message
function notifyString (type, title, message) {
  Notification({
    type: type,
    title: title,
    message: message
  })
}
// Notify Array Of Message
function notifyArray (type, title, messages) {
  let HTMLMessages = '<li>' + _.map(messages).join('<li></li>') + '</li>'
  Notification({
    type: type,
    title: title,
    dangerouslyUseHTMLString: true,
    message: HTMLMessages
  })
}
