import axios from 'axios'
import qs from 'qs'
import {redirectPath} from '@/router/routing'
import {AUTH_LOGIN_URL, AUTH_LOGOUT_URL, AUTH_GOOGLE_LOGIN_URL, HOME_PATH, LOGIN_PATH, SIGN_OUT_SUCCESS_TEXT, SIGN_IN_SUCCESS_TEXT, COMMON_ERROR_TEXT, SIGN_IN_PARAMS_INVALID_TEXT} from '@/services/const'
import {notifySuccess, notifyError} from '@/services/notify'

// Sign out
export function signOut () {
  axios({
    method: 'options',
    url: AUTH_LOGOUT_URL,
    responseType: 'json',
    headers: getLogOutHeaders(),
    data: {}
  })
    .then(function (response) {
      removeCredentials()
      notifySuccess(SIGN_OUT_SUCCESS_TEXT)
      redirectPath(LOGIN_PATH)
    })
    .catch(function () {
      removeCredentials()
      redirectPath(LOGIN_PATH)
    })
}

// Sign in
export function signIn (email, password) {
  if (email !== '' && password !== '') {
    axios({
      method: 'post',
      url: AUTH_LOGIN_URL,
      responseType: 'json',
      data: {
        email: email,
        password: password
      }
    })
      .then(function (response) {
        notifySuccess(SIGN_IN_SUCCESS_TEXT)
        removeCredentials()
        addCredentials(response.headers)
        redirectPath(HOME_PATH)
      })
      .catch(function (error) {
        notifyError(error.response.data.errors || COMMON_ERROR_TEXT)
      })
  } else {
    notifyError(SIGN_IN_PARAMS_INVALID_TEXT)
  }
}

// Sign in with Google
export function signInGoogle (gauth) {
  gauth.$gAuth.signIn()
    .then(GoogleUser => {
      let authParams = {
        profileObj: {
          googleId: GoogleUser.El,
          name: GoogleUser.w3.ig,
          email: GoogleUser.w3.U3
        },
        tokenObj: {
          id_token: GoogleUser.Zi.id_token,
          access_token: GoogleUser.Zi.access_token
        },
        provider: GoogleUser.Zi.idpId
      }
      axios({
        method: 'get',
        url: AUTH_GOOGLE_LOGIN_URL,
        responseType: 'json',
        params: authParams,
        paramsSerializer: function (params) {
          return qs.stringify(params, {arrayFormat: 'brackets'})
        }
      })
        .then(function (response) {
          notifySuccess(SIGN_IN_SUCCESS_TEXT)
          removeCredentials()
          addCredentials(response.data)
          redirectPath(HOME_PATH)
        })
        .catch(function (error) {
          notifyError(error)
        })
    })
    .catch(error => {
      console.log(error)
    })
}

// Add Credentials to localStorage
export function addCredentials (credentials) {
  localStorage.setItem('access_token', credentials['access-token'])
  localStorage.setItem('client', credentials['client'])
  localStorage.setItem('uid', credentials['uid'])
}

// Get current Credentials
export function getCredentials () {
  return {
    'access-token': localStorage.getItem('access_token'),
    'client': localStorage.getItem('client'),
    'uid': localStorage.getItem('uid')
  }
}

// Remove current Credentials
export function removeCredentials () {
  localStorage.removeItem('access_token')
  localStorage.removeItem('client')
  localStorage.removeItem('uid')
  localStorage.removeItem('service_id')
  localStorage.removeItem('environment_id')
}

// Get Credentials existed
export function isAuthenticated () {
  const credentials = getCredentials()
  return credentials['access-token'] && credentials['uid'] && credentials['client']
}

// Get requirement Header for Log Out
function getLogOutHeaders () {
  let logOutObj = getCredentials()
  Object.assign(logOutObj, {'token-type': 'Bearer'})
  return logOutObj
}
