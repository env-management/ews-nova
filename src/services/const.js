// Constant URL
export const BACKGROUND_LOGIN_URL = 'https://dvkhfbm6djrbs.cloudfront.net/592bcbf6fb314a000bf57124/root/videoplayback.mp4'
export const GITLAB_BASE_URL = 'https://git.edumall.io'
export const FAKE_API_POST_URL = 'https://jsonplaceholder.typicode.com/posts/'

// Proxy Path
export const CORE_NOVA_BASE_URL = process.env.CORE_NOVA_BASE_URL || '/sys/nova'
export const CORE_AUTHSERVICE_BASE_URL = process.env.CORE_AUTHSERVICE_BASE_URL || '/sys/authservice'

// Route Path
export const HOME_PATH = '/'
export const LOGIN_PATH = '/login'
export const ENVIRONMENT_PATH = '/environment'
export const SERVICE_PATH = '/service'
export const VARIABLE_PATH = '/variable'
export const ENVFILE_PATH = '/envfile'
export const ACCOUNT_PATH = '/account'
export const DEPARTMENT_PATH = '/department'
export const COMPANY_PATH = '/company'
export const ROLE_PATH = '/role'

// URL
export const AUTH_LOGIN_URL = CORE_AUTHSERVICE_BASE_URL + '/auth/sign_in'
export const AUTH_LOGOUT_URL = CORE_AUTHSERVICE_BASE_URL + '/auth/sign_out'
export const AUTH_GOOGLE_LOGIN_URL = CORE_AUTHSERVICE_BASE_URL + '/auth/google_oauth2/callback'

// Resource URL
export const RESOURCE_ENVIRONMENT_PATH = CORE_NOVA_BASE_URL + '/api/environment'
export const RESOURCE_VARIABLE_PATH = CORE_NOVA_BASE_URL + '/api/variable'
export const RESOURCE_SERVICE_PATH = CORE_NOVA_BASE_URL + '/api/service'
export const RESOURCE_RELEASE_PATH = CORE_NOVA_BASE_URL + '/api/release'
export const RESOURCE_SHOW_ENVFILE_PATH = CORE_NOVA_BASE_URL + '/api/list_compcond'
export const RESOURCE_UPLOAD_ENVFILE_PATH = CORE_NOVA_BASE_URL + '/api/upload'
export const RESOURCE_DOWNLOAD_ENVFILE_PATH = CORE_NOVA_BASE_URL + '/api/generate'
export const RESOURCE_ACCOUNT_PATH = CORE_AUTHSERVICE_BASE_URL + '/api/v01/accounts'
export const RESOURCE_DEPARTMENT_PATH = CORE_AUTHSERVICE_BASE_URL + '/api/v01/departments'
export const RESOURCE_COMPANY_PATH = CORE_AUTHSERVICE_BASE_URL + '/api/v01/companies'
export const RESOURCE_ROLE_PATH = CORE_AUTHSERVICE_BASE_URL + '/api/v01/roles'
export const RESOURCE_ADMINROLE_PATH = CORE_AUTHSERVICE_BASE_URL + '/api/v01/adminroles'
export const RESOURCE_ENTITY_PATH = CORE_AUTHSERVICE_BASE_URL + '/api/v01/entities'

// Resource Extended Fields
export const RESOURCE_VARIABLE_EXTENDED_FIELDS = 'environment{},service{}'
export const RESOURCE_RELEASE_EXTENDED_FIELDS = 'environment{},service{}'
export const RESOURCE_DEPARTMENT_EXTENDED_FIELDS = 'sup_department{},company{}'
export const RESOURCE_ACCOUNT_EXTENDED_FIELDS = 'department{},company{},role{},adminrole{}'

// Resource Filter Fields
export const RESOURCES_VARIABLES_FILTER_FIELDS = ['service_id', 'environment_id']
export const RESOURCES_DEPARTMENTS_FILTER_FIELDS = ['sup_department_id', 'company_id']
export const RESOURCES_ACCOUNTS_FILTER_FIELDS = ['department_id', 'company_id', 'role_id', 'adminrole_id']

// Resource Entity
export const RESOURCE_ENTITY_MODELS = ['create', 'read', 'update', 'delete']
export const RESOURCE_ENTITY_VALUES = ['none', 'user', 'bu', 'fbu', 'org']

// Sidebar Content
export const SIDEBAR_CONTENT = {
  'ENV-VAR MANAGEMENT': {
    'Service': [SERVICE_PATH, {color: ''}],
    'Environment': [ENVIRONMENT_PATH, {color: ''}],
    'Variable': [VARIABLE_PATH, {color: ''}],
    'Env File': [ENVFILE_PATH, {color: ''}]
  },
  'ACCOUNT MANAGEMENT': {
    'Account': [ACCOUNT_PATH, {color: ''}],
    'Department': [DEPARTMENT_PATH, {color: ''}],
    'Company': [COMPANY_PATH, {color: ''}],
    'Role': [ROLE_PATH, {color: ''}]
  }
}

// Datetime Format
export const DATETIME_FORMAT = 'DD.MM.YYYY - hh:mm A'

// Text
export const LOGIN_TITLE_TEXT = 'Environment Variables Management'
export const BUTTON_SIGN_IN_TEXT = 'SIGN IN'
export const BUTTON_SIGN_IN_GOOGLE_TEXT = 'CONTINUE WITH GOOGLE'
export const BUTTON_SIGN_OUT_TEXT = 'Log Out'
export const SIGN_IN_SUCCESS_TEXT = 'Sign in successful'
export const SIGN_OUT_SUCCESS_TEXT = 'Log out successful'
export const COMMON_ERROR_TEXT = 'Something went wrong'
export const SIGN_IN_PARAMS_INVALID_TEXT = 'Both email and password must be present'
export const FOOTER_TEXT = 'Copyright © 2018 TST Edumall. All rights reserved.'
export const SIDEBAR_SETTING_TEXT = 'SETTINGS'
export const SAVE_RECORD_TEXT = 'SAVE'
export const REFRESH_RECORD_TEXT = 'REFRESH'
export const DOWNLOAD_RECORD_TEXT = 'DOWNLOAD'
export const COMMON_SEARCH_PLACEHOLDER_TEXT = 'Search Name, Project Path, Key, Value, ...'
export const BUTTON_CREATE_NEW_TEXT = 'Create New'
export const BUTTON_MODIFY_ALL_TEXT = 'Modify All'
export const BUTTON_DELETE_ALL_TEXT = 'Delete All'
export const UPDATE_RECORD_SUCCESS_TEXT = 'Update successful'
export const CREATE_RECORD_SUCCESS_TEXT = 'Create successful'
export const POPOVER_DELETE_RECORD_TITLE_TEXT = 'Are you sure?'
export const POPOVER_DELETE_RECORD_CONFIRM_BUTTON_TEXT = 'Confirm'
export const DELETE_RECORD_SUCCESS_TEXT = 'Delete successful'
export const DIALOG_TITLES_CREATE_SERVICE_TEXT = 'Create New Service'
export const DIALOG_TITLES_CREATE_ENVIRONMENT_TEXT = 'Create New Environment'
export const DIALOG_TITLES_CREATE_VARIABLE_TEXT = 'Create New Variable'
export const DIALOG_TITLES_CREATE_COMPANY_TEXT = 'Create New Company'
export const DIALOG_TITLES_CREATE_DEPARTMENT_TEXT = 'Create New Department'
export const DIALOG_TITLES_CREATE_ACCOUNT_TEXT = 'Create New Account'
export const DIALOG_TITLES_CREATE_ROLE_TEXT = 'Create New Role'
export const DIALOG_TITLES_MODIFY_SERVICE_TEXT = 'Modify All Services'
export const DIALOG_TITLES_MODIFY_ENVIRONMENT_TEXT = 'Modify All Environments'
export const DIALOG_TITLES_MODIFY_VARIABLE_TEXT = 'Modify All Variables'
export const DIALOG_TITLES_MODIFY_DEPARTMENT_TEXT = 'Modify All Department'
export const DIALOG_TITLES_MODIFY_ACCOUNT_TEXT = 'Modify All Account'
export const DIALOG_TITLES_MODIFY_ROLE_TEXT = 'Modify All Role'
export const MESSAGEBOX_DELETE_ALL_VARIABLES_CONTENT = 'This will permanently delete all environment variables, are you sure?'
export const MESSAGEBOX_DELETE_ALL_VARIABLES_CONFIG = {
  confirmButtonText: 'Delete All',
  cancelButtonText: 'Cancel',
  type: 'warning'
}
export const RECORDS_HAS_BEEN_DELETED_TEXT = 'Records has been deleted'
export const BUTTON_IMPORT_FROM_FILE_TEXT = 'IMPORT FROM FILE'
export const BUTTON_LIST_RELEASE_TEXT = 'LIST RELEASE'
export const DIALOG_TITLES_IMPORT_FROM_FILE_TEXT = 'Import Evironment Variables From File'
export const DIALOG_TITLES_LIST_RELEASE_TEXT = 'List Release'
export const ENV_FILE_EDITOR_UPLOAD_TEXT = 'Drop .env file here or click to upload'
export const DEFAULT_CODEEDITOR_CONTENT = '# Preview .env file\nENV_KEY=env_variable\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n'
export const UPLOAD_ENVFILE_BUTTON_TEXT = 'Create/Update Env File'
export const UPLOAD_ENVFILE_SUCCESS_TEXT = 'Import variables from file successful'
export const CREATE_TAG_BUTTON_TEXT = 'CREATE TAG'
export const ERROR_ENVFILE_NOT_AVAILABLE_TEXT = 'Env File Not Available'
export const PASSWORD_NOT_FOUND_TEXT = `Password can't be blank`
export const ADMINROLE_NOT_FOUND_TEXT = `Adminrole can't be blank`
export const ROLE_NOT_FOUND_TEXT = `Role can't be blank`
export const PASSWORD_LENGTH_TOO_SHORT_TEXT = `Password is too short (minimum is 8 characters)`
export const EDIT_ROLE_TEXT = 'Edit Role'
export const EDIT_ROLE_ASSIGNMENT_TEST = 'Role Assignment'
export const ADD_ENTITY_TEXT = 'ADD ENTITY'
export const DIALOG_TITLES_ADD_ENTITY_TEXT = 'Add Entity'
export const ENTITY_MESSAGE_BOX_TITLE_TEXT = 'Entity Info'
export const RESTORE_ENV_TEXT = 'RESTORE TO CURRENT RELEASE'

// Color
export const DEFAULT_COLOR = '#409EFF'
export const SUCCESS_COLOR = '#67C23A'
export const WARNING_COLOR = '#E6A23C'
export const DANGER_COLOR = '#F56C6C'
export const INFO_COLOR = '#909399'
export const REGULAR_COLOR = '#606266'
export const PRIMARY_COLOR = '#303133'
export const SECONDARY_COLOR = '#909399'
export const PLACEHOLDER_COLOR = '#C0C4CC'
export const EXTRALIGHT_COLOR = '#FFFFFF'

// Default Datatable
export const DEFAULT_DATA = []
export const DEFAULT_LAYOUT = 'tool, pagination, table'
export const DEFAULT_PAGINATE = {
  pageSizes: [5, 10, 15, 20]
}
export const DEFAULT_TOTAL_RECORDS = 0
export const DEFAULT_CURRENT_PAGE = 1
export const DEFAULT_PAGE_SIZE = 10

// Customize Datatable
export const DATATABLE_SERVICE = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Service Name',
    editable: true
  },
  {
    prop: 'project_path',
    label: 'Gitlab Project Path',
    editable: true
  },
  {
    prop: 'created_at',
    label: 'Created At',
    editable: false
  },
  {
    prop: 'updated_at',
    label: 'Updated At',
    editable: false
  }
]
export const DATATABLE_ENVIRONMENT = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Environment',
    editable: true
  },
  {
    prop: 'created_at',
    label: 'Created At',
    editable: false
  },
  {
    prop: 'updated_at',
    label: 'Updated At',
    editable: false
  }
]
export const DATATABLE_VARIABLE = [
  {
    prop: 'key',
    label: 'Key',
    editable: true
  },
  {
    prop: 'value',
    label: 'Value',
    editable: true
  },
  {
    prop: 'service_id',
    label: 'Service',
    editable: true
  },
  {
    prop: 'environment_id',
    label: 'Environment',
    editable: true
  },
  {
    prop: 'created_at',
    label: 'Created',
    editable: false
  },
  {
    prop: 'updated_at',
    label: 'Updated',
    editable: false
  }
]
export const DATATABLE_DEPARTMENT = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Department Name',
    editable: true
  },
  {
    prop: 'description',
    label: 'Description',
    editable: true
  },
  {
    prop: 'sup_department_id',
    label: 'Super Department',
    editable: true
  },
  {
    prop: 'company_id',
    label: 'Company',
    editable: true
  }
]
export const DATATABLE_COMPANY = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Company Name',
    editable: true
  },
  {
    prop: 'description',
    label: 'Description',
    editable: true
  },
  {
    prop: 'created_at',
    label: 'Created At',
    editable: false
  },
  {
    prop: 'updated_at',
    label: 'Updated At',
    editable: false
  }
]
export const DATATABLE_ACCOUNT = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Account Name',
    editable: true
  },
  {
    prop: 'email',
    label: 'Email',
    editable: false
  },
  {
    prop: 'role_id',
    label: 'Role',
    editable: true
  },
  {
    prop: 'adminrole_id',
    label: 'Admin Role',
    editable: true
  },
  {
    prop: 'department_id',
    label: 'Department',
    editable: true
  },
  {
    prop: 'company_id',
    label: 'Company',
    editable: true
  }
]
export const DATATABLE_CREATE_ACCOUNT = [
  {
    prop: 'name',
    label: 'Account Name',
    editable: true
  },
  {
    prop: 'email',
    label: 'Email',
    editable: true
  },
  {
    prop: 'password',
    label: 'Password',
    editable: true
  },
  {
    prop: 'role_id',
    label: 'Role',
    editable: true
  },
  {
    prop: 'adminrole_id',
    label: 'Admin Role',
    editable: true
  },
  {
    prop: 'department_id',
    label: 'Department',
    editable: true
  },
  {
    prop: 'company_id',
    label: 'Company',
    editable: true
  }
]
export const DATATABLE_UPDATE_ACCOUNT = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Account Name',
    editable: true
  },
  {
    prop: 'password',
    label: 'Password',
    editable: true
  },
  {
    prop: 'role',
    label: 'Role',
    editable: true
  },
  {
    prop: 'adminrole',
    label: 'Admin Role',
    editable: true
  },
  {
    prop: 'department',
    label: 'Department',
    editable: true
  },
  {
    prop: 'company',
    label: 'Company',
    editable: true
  }
]
export const DATATABLE_ROLE = [
  {
    prop: 'id',
    label: 'NO.',
    editable: false
  },
  {
    prop: 'name',
    label: 'Role',
    editable: true
  },
  {
    prop: 'created_at',
    label: 'Created At',
    editable: false
  },
  {
    prop: 'updated_at',
    label: 'Updated At',
    editable: false
  }
]

// Gauth Configuration
export const GOOGLE_CLIENT_ID = '999705440626-aqlbmgnl8jsj3kkfm3pogr0r6iu2hcgr.apps.googleusercontent.com'
export const GAUTH_OPTIONS = {
  clientId: GOOGLE_CLIENT_ID,
  scope: 'profile email',
  prompt: 'select_account'
}
