import { Loading } from 'element-ui'

const loadingOptions = {
  lock: true,
  fullscreen: true
}

// open loading, to close loading use loading().close()
export function loading () {
  return Loading.service(loadingOptions)
}
