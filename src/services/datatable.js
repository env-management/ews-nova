import _ from 'lodash'
import moment from 'moment'
import {DEFAULT_PAGINATE, DATETIME_FORMAT} from '@/services/const'

// Render DataTable from data
export function renderTable (dataTable, data) {
  dataTable.data = formatRecords(data.data.records)
  dataTable.total = data.data.filters.paging.record_total
  dataTable.currentPage = data.data.filters.paging.page
  dataTable.pageSize = data.data.filters.paging.per_page
  let defaultPaginate = _.assign([], DEFAULT_PAGINATE.pageSizes)
  _.remove(defaultPaginate, function (n) {
    return n >= dataTable.total
  })
  defaultPaginate.push(dataTable.total)
  dataTable.paginate = _.assign({}, {pageSizes: defaultPaginate})
}

// Create Params for DataTable
export function paramsTable (queryInfo, fullSearchParams) {
  let params = {}
  params = paginateParams(queryInfo, params)
  params = compoundFiltersParams(queryInfo, params)
  params = sortParams(queryInfo, params)
  params = filterFullSearchParams(fullSearchParams, params)
  return params
}

// Format Records for rendering DataTable
function formatRecords (records) {
  let formattedRecords = formatTimeRecords(records)
  return formattedRecords
}

// Format Time/Datetime columns of Records for rendering DataTable
function formatTimeRecords (records) {
  let timeRecords = []
  records.forEach(function (val, index) {
    timeRecords[index] = val
    if (!_.isNil(timeRecords[index]['created_at'])) {
      timeRecords[index]['created_at'] = moment(timeRecords[index]['created_at']).format(DATETIME_FORMAT)
    }
    if (!_.isNil(timeRecords[index]['updated_at'])) {
      timeRecords[index]['updated_at'] = moment(timeRecords[index]['updated_at']).format(DATETIME_FORMAT)
    }
  })
  return timeRecords
}

// Paginate Params
function paginateParams (queryInfo, params) {
  let paginatedParams = _.assign({}, params)
  paginatedParams.page = queryInfo.page
  paginatedParams.per_page = queryInfo.pageSize
  return paginatedParams
}

// Sort Params
function sortParams (queryInfo, params) {
  let sortingParams = _.assign({}, params)
  if (queryInfo.type === 'sort') {
    switch (queryInfo.sort.order) {
      case 'ascending':
        sortingParams.orders = [ `${queryInfo.sort.prop}.asc` ]
        break
      case 'descending':
        sortingParams.orders = [ `${queryInfo.sort.prop}.desc` ]
        break
      default:
        _.omit(sortingParams, ['orders'])
        break
    }
  }
  return sortingParams
}

// add filter for Full Search Option
function filterFullSearchParams (fullSearchParams, params) {
  let searchedParams = _.assign({}, params)
  if (!_.isNil(fullSearchParams)) {
    searchedParams.full_search = fullSearchParams
  }
  return searchedParams
}

// add compound Filters
function compoundFiltersParams (queryInfo, params) {
  let compoundFilters = queryInfo.compoundFilters
  let compoundParams = _.assign({}, params)
  _.map(compoundFilters, function (value, key) {
    compoundParams[key] = value
  })
  return compoundParams
}
