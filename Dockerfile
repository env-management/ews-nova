FROM node:lts-alpine as build_static
RUN apk add --update python make g++
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build
FROM nginx:alpine
RUN apk add --update --no-cache libintl gettext
ADD ENVFILE nginx.conf /
RUN export $(cat ENVFILE | xargs) && \
    envsubst "`printf '${%s} ' $(env|cut -d'=' -f1)`" < nginx.conf > /etc/nginx/nginx.conf
COPY --from=build_static /app/dist /usr/share/nginx/html